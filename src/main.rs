extern crate regex;

mod tokens;

use std::collections::HashMap;
use std::io::{self, Write};
use std::num;


fn main() {
    repl();
}

fn repl() {
    let in_handler = io::stdin();
    let mut out_handler = io::stdout();

    loop {
        // Read
        print!("rusty >> ");
        out_handler.flush().expect("Failed to flush stdout.");
        let mut input = String::new();
        in_handler
            .read_line(&mut input)
            .expect("Couldnt read user input from stdin.");
        input = input.trim().to_string();

        // Eval
        if input == "exit" {
            break;
        }

        let tokens = tokenise(input);
        let env = get_default_env();
        let (ast, _) = parse_tokens(&tokens).expect("Failed to parse.");
        let eval_result = evaluate(&ast, &env);
        match eval_result {
            Ok(value) => match value {
               Lexp::Sym(value) => println!("Symbol Found: {}", value),
               Lexp::Num(value) => println!("Number: {}", value),
               Lexp::List(value) => println!("List Found"),
               Lexp::Func(value) =>  println!("Function Found"),
               _ => println!("Error occured."),
            },
            Err(err) => println!("Error occured."),
        }
        
    }
    println!("Goodbye!")
}

fn tokenise(expr: String) -> Vec<String> {
    expr.replace("(", " ( ")
        .replace(")", " ) ")
        .split_whitespace()
        .map(|x| x.to_string())
        .collect()
}

fn parse_tokens<'a>(tokens: &[String]) -> Result<(Lexp, &[String]), RLispErr> {
    let (token, rest) = tokens
        .split_first()
        .ok_or(RLispErr::Reason("could not get token".to_string()))?;
    
    let env = get_default_env();

    match &token[..] {
        "(" => read_seq(rest),
        ")" => Err(RLispErr::Reason("unexpected `)`".to_string())),
        _ => Ok((parse_atom(token), rest)),
    }
}

fn read_seq<'a>(tokens: &[String]) -> Result<(Lexp, &[String]), RLispErr> {
    let mut res: Vec<Lexp> = vec![];
    let mut xs = tokens;
    
    loop {
        let (next_token, rest) = xs
            .split_first()
            .ok_or(RLispErr::Reason("could not find closing `)`".to_string()))?;

        match &next_token[..] {
            ")" => return Ok((Lexp::List(res), rest)),
            _ => {},
        };

        let (exp, new_xs) = parse_tokens(&xs)?;
        res.push(exp);
        xs = new_xs;
        
    }
}

fn parse_atom(token: &str) -> Lexp {
    let potential_float: Result<f64, num::ParseFloatError> = token.parse();
    match potential_float {
        Ok(v) => Lexp::Num(v),
        Err(_) => Lexp::Sym(token.to_string().clone()),
    }
}

fn get_default_env() -> Lenv {
    let mut env = Lenv::new();
    
    env.insert(
        "+".to_string(),
        Lexp::Func(
            |args: &[Lexp]| -> Result<Lexp, RLispErr> {
                let sum = parse_floats(args)?.iter().fold(0.0, |sum, a| sum + a);

                Ok(Lexp::Num(sum))
            }
        )
    );

    env.insert(
        "-".to_string(),
        Lexp::Func(
            |args: &[Lexp]| -> Result<Lexp, RLispErr> {
                let values = parse_floats(args)?;
                let first = values.first().ok_or(RLispErr::Reason("Expected numbers, none found.".to_string()))?;
                let sum_rest = values[1..].iter().fold(0.0, |sum, a| sum + a);

                Ok(Lexp::Num(first - sum_rest))
            }
        )
    );

    env
}

fn parse_float(exp: &Lexp) -> Result<f64, RLispErr> {
    match exp {
        Lexp::Num(exp) => Ok(*exp),
        _ => Err(RLispErr::Reason("Mismatched value, expected number.".to_string())),
    }
}

fn parse_floats(exps: &[Lexp]) -> Result<Vec<f64>, RLispErr> {
    exps.iter()
        .map(|exp| parse_float(exp))
        .collect()
}

fn evaluate(exp: &Lexp, env: &Lenv) -> Result<Lexp, RLispErr> {
    match exp {
        Lexp::Sym(exp) => {
            env.get(exp)
                .ok_or(
                    RLispErr::Reason(
                        "Unexpected Symbol, found expected '+' or '-'.".to_string()
                    )
                )
                .map(|x| x.clone())
        },
        Lexp::Num(_a) => Ok(exp.clone()),
        Lexp::List(exp) => {
            evaluate_func_call(exp, env)
        },
        Lexp::Func(_) => Err(
            RLispErr::Reason("unexpected form".to_string())
        ),
    }
    
}

fn evaluate_func_call<'a>(exp: &Vec<Lexp>, env: &Lenv) -> Result<Lexp, RLispErr> {
    let function_name = &exp.first()
                            .ok_or(
                                RLispErr::Reason("Expected list with elements".to_string()
                            ))?;
    let args = &exp[1..];
    let func = evaluate(function_name, env)?;
    match func {
        Lexp::Func(func) => {
            let args_eval = args.iter()
                            .map(|x| evaluate(x, env))
                            .collect::<Result<Vec<Lexp>, RLispErr>>();
            func(&args_eval?[..])
        },
        _ => Err(RLispErr::Reason("Unexpected parse error.".to_string()))
        ,
    }
}

fn evaluate_defun<'a>(exp: &Vec<Lexp>, env: &'a Lenv) -> Result<(&Vec<Lexp>, &'a Lenv), RLispErr> {
    let first_exp = &exp.first()
                        .ok_or(
                            RLispErr::Reason("Expected list with elements".to_string()
                        )).unwrap();
    match first_exp {
        Lexp::Sym(first_exp) => match first_exp {
            "defun" => {
                
            }
            _ => evaluate_func_call(exp, env)
        },
        _ => Err(RLispErr::Reason("Invalid syntax.".to_string()))
    }
}


type Lenv = HashMap<String, Lexp>;

#[derive(Clone)]
enum Lexp {
    Num(f64),
    Sym(String),
    List(Vec<Lexp>),
    Func(fn(&[Lexp]) -> Result<Lexp, RLispErr>),
}

#[derive(Debug)]
enum RLispErr {
    Reason(String),
}


